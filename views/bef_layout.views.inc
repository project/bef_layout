<?php
/**
 * @file
 * Provides views exposed form plugins.
 */

/**
 * Implements hook_views_plugins().
 */
function bef_layout_views_plugins() {
  $plugins = array('exposed_form' => array());
  $plugins['exposed_form']['bef_basic_layout'] = array(
    'title' => t('Basic (with layout)'),
    'help' => t('Adds layout settings for Exposed form'),
    'handler' => 'bef_layout_exposed_form_plugin_basic',
    'uses row plugin' => FALSE,
    'uses fields' => TRUE,
    'uses options' => TRUE,
    'help topic' => 'exposed-form-basic',
    'type' => 'normal',
    'parent' => 'views_plugin_exposed_form_basic',
  );

  if (module_exists('better_exposed_filters')) {
    $plugins['exposed_form']['bef_layout_bef'] = array(
      'title' => t('Better Exposed Filters (with layout)'),
      'help' => t('Adds layout settings to BEF module'),
      'handler' => 'bef_layout_exposed_form_plugin_bef',
      'uses row plugin' => FALSE,
      'uses fields' => TRUE,
      'uses options' => TRUE,
      'help topic' => 'exposed-form-basic',
      'type' => 'normal',
      'parent' => 'better_exposed_filters',
    );
  }
  return $plugins;
}
