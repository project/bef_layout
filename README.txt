CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Credits
 * Maintainers


INTRODUCTION
------------
Inspired by Module filter module I've created such interface for Rules module.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/vefl

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/vefl


FEATURES
--------
 * Vertical tabs (generated from Rules tags).
 * Quick search by Rules description, events and plugins.
 * Filter by enabled/disabled rules.


REQUIREMENTS
------------
This module requires the following modules:
 * Views (https://drupal.org/project/views)


INSTALLATION
------------
Install as you would normally install a contributed drupal module.
See: https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------
* For site-builders:
  -On views edit page find Exposed form section.
  -Chose Basic (with layout) or Better Exposed Filters (with layout) Exposed form style.
  -On Exposed form settings page you will see Layout settings fieldset now.
  -Chose Layout and click Change button.
  -Set in which region each exposed filter will be outputted.
  -Click Apply and have fun.
* For developers:
  -You can provide custom layouts, use hook for it.
  -


CREDITS
-------
The project sponsored by
Bright Solutions GmbH (https://www.drupal.org/node/1469032).


MAINTAINERS
-----------
Current maintainers:
 * Sergey Korzh (korgik) - https://drupal.org/user/813560
